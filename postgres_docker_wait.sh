#!/usr/bin/env bash
set -x
command="$@"

until PGPASSWORD=$POSTGRES_PASSWORD psql -v ON_ERROR_STOP=1 \
  --host "$POSTGRES_HOST" \
  --port 5432 \
  --username "$POSTGRES_USER" \
  --dbname "$POSTGRES_DB" \
  --command '\q'; do

  >&2 echo "sleep for postgres"
  sleep 3
done

PGPASSWORD=$POSTGRES_PASSWORD psql \
  --host "$POSTGRES_HOST" \
  --port 5432 \
  --username "$POSTGRES_USER" \
  --dbname "$POSTGRES_DB" \
  --command "CREATE EXTENSION IF NOT EXISTS citext;
CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";
CREATE EXTENSION IF NOT EXISTS rum;"

# init db
/opt/pleroma/bin/pleroma_ctl migrate

>&2 echo "postgres connected"
exec $command
