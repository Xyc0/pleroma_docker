FROM ubuntu:18.04
ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
ENV MIX_ENV=prod
ENV HOME=/opt/pleroma

RUN apt-get update && apt-get install --yes \
    imagemagick \
    libncurses5 \
    postgresql-client \
    unzip \
    && apt-get clean

ADD https://git.pleroma.social/api/v4/projects/2/jobs/artifacts/stable/download?job=amd64 /tmp/pleroma.zip
RUN unzip /tmp/pleroma.zip -d /tmp/ && mv /tmp/release /opt/pleroma

ADD postgres_docker_wait.sh /pleroma/postgres_docker_wait.sh

EXPOSE 4000
VOLUME /var/lib/pleroma/uploads
VOLUME /var/lib/pleroma/static
VOLUME /etc/pleroma
CMD ["/pleroma/postgres_docker_wait.sh", "/opt/pleroma/bin/pleroma", "start"]
